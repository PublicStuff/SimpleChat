﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Soap;
using SimpleChat.Chat;
using SimpleChat.Files;
using SimpleChat.Network;
using System.Threading;
using System.Net;
using System.Collections.ObjectModel;

namespace TestConsole
{
    class Program
    {
        static object locker = new object();
        static bool stop = false;

        static void Main(string[] args)
        {
            ObservableCollection<string> asd = new ObservableCollection<string>();

            for (int i = 0; i < 20; i++)
            {
                asd.Add(i.ToString());
            }


            FileIO fileIO = new FileIO();
            NetworkManager manager = new NetworkManager();

            manager.Connected += Manager_Connected;
            manager.Disconnected += Manager_Disconnected;
            manager.Listening += Manager_Listening;
            manager.MessageReceived += Manager_MessageReceived;

            while (true)
            {
                int inp = 0;

                do
                {
                    Console.Clear();
                    Console.WriteLine("(1): Connect  || (2): Await connection");
                    int.TryParse(Console.ReadLine(), out inp);
                }
                while (inp != 1 && inp != 2);

                switch (inp)
                {
                    case 1:
                        Console.WriteLine("Enter the IP, press enter enter the port and press enter to connect");

                        try
                        {
                            manager.Connect(Console.ReadLine(), int.Parse(Console.ReadLine()));
                        }
                        catch
                        {
                            continue;
                        }

                        while (!stop)
                        {
                            if (Console.KeyAvailable)
                            {
                                manager.Send(new Message(Console.ReadLine(), true));
                            }

                            Thread.Sleep(100);
                        }

                        break;
                    case 2:
                        manager.StartListening();

                        while (!stop)
                        {
                            if (Console.KeyAvailable)
                            {
                                manager.Send(new Message(Console.ReadLine(), true));
                            }

                            Thread.Sleep(100);
                        }

                        break;
                    default:
                        break;
                }
            }
        }

        private static void Manager_MessageReceived(object sender, NetworkIOMessageReceivedEventArgs e)
        {
            WriteLine(e.Message.Text);
        }

        private static void WriteLine(string v)
        {
            lock (locker)
            {
                Console.WriteLine(v);
            }
        }

        private static void Manager_Listening(object sender, NetworkManagerListeningEventArgs e)
        {
            WriteLine("Started listening");
        }

        private static void Manager_Disconnected(object sender, NetworkManagerDisconnectedEventArgs e)
        {
            WriteLine("Disconnected");
        }

        private static void Manager_Connected(object sender, NetworkManagerConnectedEventArgs e)
        {
            WriteLine("Connected");
        }
    }
}
