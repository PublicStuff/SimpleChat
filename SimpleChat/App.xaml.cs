﻿using SimpleChat.VM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SimpleChat
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            this.ManagerVM = new ChatManagerVM();
        }
        
        internal ChatManagerVM ManagerVM
        {
            get;
        }
    }
}
