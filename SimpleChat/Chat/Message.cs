﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleChat.Chat
{
    [Serializable()]
    public class Message
    {
        public Message(string text, bool own)
        {
            this.Timestamp = DateTime.Now;
            this.Text = text;
            this.OwnMessage = own;
        }

        public string Text
        {
            get;
            private set;
        }

        public DateTime Timestamp
        {
            get;
        }

        public bool OwnMessage
        {
            get;
            set;
        }
    }
}