﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace SimpleChat.Chat
{
    [Serializable()]
    public class ChatLog
    {
        private string userName;

        public ChatLog(string username = "")
        {
            this.MessageList = new List<Message>();
            this.UserName = username;
        }

        public ChatLog(string username, string partnername, string partnerIp) : this(username)
        {
            this.PartnerName = partnername;
            this.PartnerIp = partnerIp;
        }

        public string PartnerName
        {
            get;
            set;
        }


        public string PartnerIp
        {
            get;
            set;
        }

        public List<Message> MessageList
        {
            get;
            set;
        }

        public string UserName
        {
            get
            {
                return this.userName;
            }

            set
            {
                this.userName = value;
            }
        }

        public void AddMessage(Message message)
        {
            // todo: might alter this later
            if (this.MessageList != null)
            {
                lock (this.MessageList)
                {
                    this.MessageList.Add(message);
                }
            }
        }
    }
}