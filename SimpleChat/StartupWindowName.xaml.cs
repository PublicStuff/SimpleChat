﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SimpleChat
{
    /// <summary>
    /// Interaction logic for StartupWindowName.xaml
    /// </summary>
    public partial class StartupWindowName : Window
    {
        public StartupWindowName()
        {
            InitializeComponent();
            this.DataContext = (Application.Current as App).ManagerVM;
            Application.Current.MainWindow.Height = (int)Math.Max(150, Math.Round(SystemParameters.PrimaryScreenHeight / 6));
            Application.Current.MainWindow.Width = (int)Math.Max(300, Math.Round(SystemParameters.PrimaryScreenWidth / 4));
        }

        private void NameBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if (NameBox.Text == null || NameBox.Text.Trim() == String.Empty)
            //{
            //    SaveNameButton.IsEnabled = false;
            //    SaveNameButton.BorderBrush = Brushes.Red;
            //}
            //else
            //{
            //    try
            //    {
            //        SaveNameButton.BorderBrush = Brushes.Green;

            //        SaveNameButton.IsEnabled = true;
            //    }
            //    catch (NullReferenceException)
            //    {
            //    }
            //}
        }

        private void PortBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void SaveNameButton_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    (Application.Current as App).ManagerVM.UserName = this.NameBox.Text;
            //}
            //catch
            //{
            //    return;
            //}

            // MessageBox.Show("Your name is:\n" + (Application.Current as App).ManagerVM.UserName + "\nYour port is:\n" + (Application.Current as App).ManagerVM.OwnPort, "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
            Window nextWindow = new ConnectionWindow();
            nextWindow.Show();
            this.Close();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //try
            //{
            //    NameBox.MaxWidth = Math.Max(1, Application.Current.MainWindow.ActualWidth - NameLabel.ActualWidth);
            //    NameBox.Width = Math.Max(1, Application.Current.MainWindow.ActualWidth - NameLabel.ActualWidth);
            //}
            //catch
            //{
            //}
        }
    }
}
