﻿using SimpleChat.VM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SimpleChat
{
    /// <summary>
    /// Interaction logic for ChatWindow.xaml
    /// </summary>
    public partial class ChatWindow : Window
    {
        private BackgroundWorker backgroundWorkerGettingName;
        private bool restart;

        public ChatWindow()
        {
            InitializeComponent();
            this.TheMainWindow.Title = (Application.Current as App).ManagerVM.TitleObject.ToString();
            this.DataContext = (Application.Current as App).ManagerVM;
            (Application.Current as App).ManagerVM.PropertyChanged += this.ManagerVM_PropertyChanged;
            (Application.Current as App).ManagerVM.Exited += this.ManagerVM_Exited;

            this.backgroundWorkerGettingName = new BackgroundWorker()
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            
            this.backgroundWorkerGettingName.DoWork += BackgroundWorkerGettingName_DoWork;
            this.backgroundWorkerGettingName.RunWorkerCompleted += BackgroundWorkerGettingName_RunWorkerCompleted;
            this.backgroundWorkerGettingName.ProgressChanged += BackgroundWorkerGettingName_ProgressChanged;

            this.StartBackgroundWorkerGettingName();
            //this.TestAnimation();
        }

        private void ManagerVM_Exited(object sender, ChatManagerVMExitedEventArgs e)
        {
            if (this.restart)
            {
                Process process = new Process()
                {
                    StartInfo = new ProcessStartInfo(System.Reflection.Assembly.GetEntryAssembly().Location)
                };

                this.TheMainWindow.Visibility = Visibility.Collapsed;

                process.Start();
            }

            this.Close();
        }

        private void StartBackgroundWorkerGettingName()
        {
            this.AnimationBox.Visibility = Visibility.Visible;
            this.ContentPanel.Visibility = Visibility.Visible;
            this.ActualGrid.Visibility = Visibility.Collapsed;

            this.Progressbar.IsIndeterminate = true;

            this.backgroundWorkerGettingName.RunWorkerAsync();
        }

        private void BackgroundWorkerGettingName_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Thread.Sleep(500);

            this.Progressbar.Value = e.ProgressPercentage;

            if (e.UserState is Tuple<string, bool> state)
            {
                //this.Progressbar.IsIndeterminate = state.Item2;
                this.AnimationTextBlock.Content = state.Item1;
            }
        }

        private void BackgroundWorkerGettingName_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Progressbar.IsIndeterminate = false;

            if (e.Error != null)
            {
                this.Progressbar.Foreground = Brushes.Red;
                var result = MessageBox.Show(e.Error.Message, "Oh snap, something went wrong.. :/\n\nDo you want to start again?", MessageBoxButton.YesNo, MessageBoxImage.Error);

                switch (result)
                {
                    case MessageBoxResult.No:
                        this.Hide();
                        (Application.Current as App).ManagerVM.Exit();
                        this.restart = false;
                        break;
                    case MessageBoxResult.Yes:
                    default:
                        this.Hide();
                        (Application.Current as App).ManagerVM.Exit();
                        this.restart = true;
                        break;
                }
            }
            else
            {
                if (e.Cancelled)
                {
                    this.Progressbar.Foreground = Brushes.Yellow;
                    var result = MessageBox.Show("You cancelled the connection.\n\n Do you want to exit the application? \n\n ['No' is not implemented yet..]", "Connection cancelled", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    switch (result)
                    {
                        case MessageBoxResult.No:
                            this.Hide();
                            (Application.Current as App).ManagerVM.Exit();
                            this.restart = true;
                            break;
                        case MessageBoxResult.Yes:
                        default:
                            this.Hide();
                            (Application.Current as App).ManagerVM.Exit();
                            this.restart = false;
                            break;
                    }
                }
                else
                {
                    this.Progressbar.Value = 100;
                    this.AnimationTextBlock.Content = "Ready!";

                    this.AnimationBox.Visibility = Visibility.Collapsed;
                    this.ContentPanel.Visibility = Visibility.Collapsed;
                    this.ActualGrid.Visibility = Visibility.Visible;
                }
            }

        }

        // This event handler is where the actual,
        // potentially time-consuming work is done.
        private void BackgroundWorkerGettingName_DoWork(object sender, DoWorkEventArgs e)
        {            
            // Get the BackgroundWorker that raised this event.
            BackgroundWorker worker = sender as BackgroundWorker;

            // Assign the result of the computation
            // to the Result property of the DoWorkEventArgs
            // object. This is will be available to the 
            // RunWorkerCompleted eventhandler.
            e.Result = (Application.Current as App).ManagerVM.SetupSession(e.Argument, worker, e);
        }

        private void ManagerVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e != null && e.PropertyName == nameof(ChatManagerVM.TitleObject))
            {
                this.TheMainWindow.Title = (Application.Current as App).ManagerVM.TitleObject.ToString();
            }
        }

        private void TestAnimation()
        {
            BeginStoryboard sb = this.FindResource("LoadingAnimationStoryboard") as BeginStoryboard;
            this.AnimationBox.Visibility = Visibility.Visible;
            this.ContentPanel.Visibility = Visibility.Visible;
            //this.AnimationTextBlock.IsEnabled = true;
            sb.Storyboard.Begin();

            sb.Storyboard.Stop();
            this.AnimationBox.Visibility = Visibility.Collapsed;
            this.ContentPanel.Visibility = Visibility.Collapsed;
        }
    }
}
