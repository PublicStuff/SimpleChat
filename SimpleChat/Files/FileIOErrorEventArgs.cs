﻿using System;

namespace SimpleChat.Files
{
    public class FileIOErrorEventArgs : EventArgs
    {
        public FileIOErrorEventArgs(string msg = "")
        {
            this.Message = msg;
        }

        public string Message { get; }
    }
}