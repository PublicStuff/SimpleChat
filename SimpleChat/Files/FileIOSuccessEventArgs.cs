﻿using System;

namespace SimpleChat.Files
{
    public class FileIOSuccessEventArgs : EventArgs
    {
        public FileIOSuccessEventArgs(string msg = "")
        {
            this.Message = msg;
        }

        public string Message { get; }
    }
}