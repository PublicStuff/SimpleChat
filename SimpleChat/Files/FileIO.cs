﻿using SimpleChat.Chat;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SimpleChat.Files
{
    public class FileIO
    {
        private readonly DirectoryInfo dir;
        private readonly string fileExtension;
        private readonly JsonSerializer jsonSerializer;

        public FileIO()
        {
            this.jsonSerializer = new JsonSerializer();

            try
            {
                this.dir = new DirectoryInfo(Directory.GetCurrentDirectory() + @"\logs\");
            }
            catch
            {
            }

            this.fileExtension = ".json";
        }

        public event EventHandler<FileIOErrorEventArgs> ErrorIO;
        public event EventHandler<FileIOSuccessEventArgs> SuccessIO;

        public bool FileOpen
        {
            get;
            private set;
        }

        public void SaveMessages(string partnerIp, string partnerName, List<Message> messages)
        {
            string tempName = partnerIp + partnerName + this.fileExtension;

            try
            {
                using (FileStream fileStream = new FileStream(this.dir + tempName, FileMode.Create, FileAccess.Write, FileShare.None))
                using (StreamWriter writer = new StreamWriter(fileStream))
                using (JsonWriter jsonWriter = new JsonTextWriter(writer))
                {
                    if (messages != null)
                    {
                        this.jsonSerializer.Serialize(jsonWriter, messages);
                    }
                }
            }
            catch
            {
                this.FireError(this, new FileIOErrorEventArgs("Cannot access file!"));
                return;
            }

            this.FireSuccess(this, new FileIOSuccessEventArgs("Sucessfully saved to file!"));
        }

        public List<Message> ReadMessages(string partnerIp, string partnerName)
        {
            string tempName = partnerIp + partnerName + this.fileExtension;
            List<Message> results = null;

            try
            {
                using (FileStream fileStream = new FileStream(this.dir + tempName, FileMode.Create, FileAccess.Write, FileShare.None))
                using (StreamReader reader = new StreamReader(fileStream))
                using (JsonReader jsonReader = new JsonTextReader(reader))
                {
                    results = (List<Message>)this.jsonSerializer.Deserialize(jsonReader);
                }
            }
            catch
            {
                this.FireError(this, new FileIOErrorEventArgs("Cannot read file!"));
                return results;
            }
            
            this.FireSuccess(this, new FileIOSuccessEventArgs());
            return results;
        }

        protected virtual void FireSuccess(object sender, FileIOSuccessEventArgs e)
        {
            this.SuccessIO?.Invoke(sender, e);
        }

        protected virtual void FireError(object sender, FileIOErrorEventArgs e)
        {
            this.ErrorIO?.Invoke(sender, e);
        }
    }
}