﻿using SimpleChat.Chat;
using System;

namespace SimpleChat.Manager
{
    public class ChatManagerChatLogMessageAddedEventArgs : EventArgs
    {
        public ChatManagerChatLogMessageAddedEventArgs(Message message)
        {
            this.Message = message;
        }

        public Message Message { get; }
    }
}