﻿using SimpleChat.Chat;
using SimpleChat.Files;
using SimpleChat.Network;
using SimpleChat.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChat.Manager
{
    public class ChatManager
    {
        private FileIO fileIO;
        private ChatLog chatLog;
        private NetworkManager networkManager;
        private bool initialized;

        public ChatManager()
        {
            this.networkManager = new NetworkManager();
            this.fileIO = new FileIO();
            this.ChatLog = new ChatLog();
            this.initialized = false;
        }

        public event EventHandler<ChatManagerChatLogMessageAddedEventArgs> MessageAdded;
        public event EventHandler<ChatManagerNewChatLogEventArgs> NewChatLog;
        //public event EventHandler<NetworkIOMessageReceivedEventArgs> MessageReceived;
        public event EventHandler<NetworkManagerListeningEventArgs> Listening;
        public event EventHandler<NetworkManagerStoppedEventArgs> Stopped;
        public event EventHandler<NetworkManagerConnectedEventArgs> Connected;
        public event EventHandler<NetworkManagerDisconnectedEventArgs> Disconnected;

        public string UserName
        {
            get
            {
                return this.chatLog.UserName ?? String.Empty;
            }

            set
            {
                if (this.chatLog == null || value == null)
                {
                    return;
                }

                this.chatLog.UserName = value;
            }
        }

        public NetworkStatus NetworkStatus
        {
            get
            {
                if (this.networkManager == null)
                {
                    return NetworkStatus.Stopped;
                }

                return this.networkManager.Status;
            }
        }

        public int Port
        {
            get;
            set;
        }

        public string HostUserName
        {
            get;
            private set;
        }

        public ChatLog ChatLog
        {
            get
            {
                return this.chatLog;
            }

            set
            {
                this.chatLog = value;
            }
        }

        public void StartListening()
        {
            lock (this.networkManager)
            {
                this.SetupNewManager();
                this.networkManager.StartListening();
            }
        }

        //protected virtual void FireMessageReceived(object sender, NetworkIOMessageReceivedEventArgs e)
        //{
        //    this.MessageReceived?.Invoke(sender, e);
        //}

        protected virtual void FireMessageAdded(object sender, ChatManagerChatLogMessageAddedEventArgs e)
        {
            this.MessageAdded?.Invoke(sender, e);
        }

        protected virtual void FireListening(object sender, NetworkManagerListeningEventArgs e)
        {
            this.Listening?.Invoke(sender, e);
        }
        protected virtual void FireNewLog(object sender, ChatManagerNewChatLogEventArgs e)
        {
            this.NewChatLog?.Invoke(sender, e);
        }

        protected virtual void FireConnected(object sender, NetworkManagerConnectedEventArgs e)
        {
            this.Connected?.Invoke(sender, e);
        }

        protected virtual void FireDisconnected(object sender, NetworkManagerDisconnectedEventArgs e)
        {
            this.Disconnected?.Invoke(sender, e);
        }

        protected virtual void FireStopped(object sender, NetworkManagerStoppedEventArgs e)
        {
            this.Stopped?.Invoke(sender, e);
        }

        private void SetupNewManager()
        {
            if (this.networkManager != null)
            {
                switch (this.networkManager.Status)
                {
                    case Network.NetworkStatus.Stopped:
                        break;
                    case Network.NetworkStatus.Listening:
                        this.networkManager.StopListening();
                        break;
                    case Network.NetworkStatus.Connected:
                        this.networkManager.Disconnect();
                        break;
                    default:
                        break;
                }

                this.networkManager.Connected -= this.NetworkManager_Connected;
                this.networkManager.Disconnected -= this.NetworkManager_Disconnected;
                this.networkManager.Listening -= this.NetworkManager_Listening;
                this.networkManager.MessageReceived -= this.NetworkManager_MessageReceived;
                this.networkManager.Stopped -= this.NetworkManager_Stopped;
            }

            this.networkManager = new NetworkManager(this.Port);
            this.networkManager.Connected += this.NetworkManager_Connected;
            this.networkManager.Disconnected += this.NetworkManager_Disconnected;
            this.networkManager.Listening += this.NetworkManager_Listening;
            this.networkManager.MessageReceived += this.NetworkManager_MessageReceived;
            this.networkManager.Stopped += this.NetworkManager_Stopped;
        }

        internal bool SendUserName()
        {
            return this.networkManager.Send(new Message(this.UserName, true));
        }

        public void Connect(string hostname, int port = 1337)
        {
            this.networkManager.Connect(hostname, port, true);
        }

        private void NetworkManager_Stopped(object sender, NetworkManagerStoppedEventArgs e)
        {
            this.FireStopped(this, e);
        }

        private void NetworkManager_MessageReceived(object sender, NetworkIOMessageReceivedEventArgs e)
        {
            if (this.initialized)
            {
                Task.Run(() =>
                {
                    this.ChatLog.AddMessage(e.Message);
                    this.FireMessageAdded(this, new ChatManagerChatLogMessageAddedEventArgs(e.Message));
                });
            }
            else
            {
                this.initialized = true;

                try
                {
                    this.HostUserName = e.Message.Text;
                }
                catch (NullReferenceException)
                {
                    this.HostUserName = "could not resolve name";
                }

                this.FireMessageAdded(this, new ChatManagerChatLogMessageAddedEventArgs(e.Message));
            }
        }

        internal bool LoadLogFor(string hostUserName, string ip)
        {
            if (this.fileIO.ReadMessages(ip, hostUserName) is List<Message> msgList)
            {
                this.ChatLog = new ChatLog(this.UserName, hostUserName, ip);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void NetworkManager_Listening(object sender, NetworkManagerListeningEventArgs e)
        {
            this.FireListening(this, e);
        }

        private void NetworkManager_Disconnected(object sender, NetworkManagerDisconnectedEventArgs e)
        {
            Task.Run(() =>
            {
                this.FireDisconnected(this, e);
            });

            this.StartListening();
        }

        private void NetworkManager_Connected(object sender, NetworkManagerConnectedEventArgs e)
        {
            this.FireConnected(this, e);
        }

        internal void SendMessage(string msg)
        {
            var message = new Message(msg, true);

            Task.Run(() =>
            {
                this.networkManager.Send(message);
            });
            
            Task.Run(() =>
            {
                this.ChatLog.AddMessage(message);
                this.FireMessageAdded(this, new ChatManagerChatLogMessageAddedEventArgs(message));
            });
        }
    }
}