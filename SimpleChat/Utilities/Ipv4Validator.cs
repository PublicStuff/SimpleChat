﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SimpleChat.Utilities
{
    public class Ipv4Validator : IValueConverter
    {
        /// <summary>
        /// Validates an ipv4-string.
        /// </summary>
        /// <param name="value">The ipv4-string.</param>
        /// <param name="targetType">The parameter is not used.</param>
        /// <param name="parameter">The parameter is not used.</param>
        /// <param name="culture">The parameter is not used.</param>
        /// <returns>True if the string is a valid ipv4 address else, false.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is string ipv4String && ipv4String.Split(new char[] { '.' }).Length == 4 && IPAddress.TryParse(ipv4String, out _);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
