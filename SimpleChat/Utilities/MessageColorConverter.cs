﻿using SimpleChat.Chat;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace SimpleChat.Utilities
{
    public class MessageColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool own)
            {
                if (own)
                {
                    return Brushes.LightBlue;
                }
                else
                {
                    return Brushes.LightGray;
                }
            }
            else
            {
                return Brushes.PaleVioletRed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
