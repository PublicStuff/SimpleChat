﻿using SimpleChat.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChat.Utilities
{
    public static class Extensions
    {
        public static bool IsBetween<T>(this T item, T start, T end)
        {
            return Comparer<T>.Default.Compare(item, start) >= 0
                && Comparer<T>.Default.Compare(item, end) <= 0;
        }

        public static bool IsValidIpv4Address(this string ipv4String)
        {
            return ipv4String != null && ipv4String.Split(new char[] { '.' }).Length == 4 && IPAddress.TryParse(ipv4String, out _);
        }        
    }
}
