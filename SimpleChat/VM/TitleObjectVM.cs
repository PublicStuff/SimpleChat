﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace SimpleChat.VM
{
    public class TitleObjectVM : INotifyPropertyChanged
    {
        private string hostUserName;
        private string hostIp;
        private string status;

        /// <summary>
        /// The <see cref="PropertyChangedEventHandler"/> of this class.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public string HostUserName
        {
            get
            {
                return this.hostUserName;
            }

            set
            {
                this.hostUserName = value;
                this.Notify(nameof(this.HostUserName));
            }
        }

        public string HostIp
        {
            get
            {
                return this.hostIp;
            }

            set
            {
                this.hostIp = value;
                this.Notify(nameof(this.HostIp));
            }
        }

        public string Status
        {
            get
            {
                return this.status;
            }

            set
            {
                this.status = value;
                this.Notify(nameof(this.Status));
            }
        }

        /// <summary>
        /// Fires the property changer event of this class, it also makes sure that the UI will be called on the UI thread.
        /// </summary>
        /// <param name="propName">The name of the property which has changed.</param>
        protected virtual void Notify([CallerMemberName]string propName = null)
        {
            // todo: check, if throws error then try with saved dispatcher at teh beginning  
            if (Application.Current.Dispatcher.CheckAccess())
            {
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(
                  DispatcherPriority.Background,
                  new Action(() => {
                      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
                  }));
            }
        }

        public override string ToString()
        {
            string res = string.Empty;

            try
            {
                res = $"[{this.HostUserName} @ {this.HostIp}]  ||  {this.Status}";
            }
            catch (NullReferenceException)
            {
            }

            return res;
        }
    }
}
