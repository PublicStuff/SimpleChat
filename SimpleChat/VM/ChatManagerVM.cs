﻿using SimpleChat.Chat;
using SimpleChat.Files;
using SimpleChat.Manager;
using SimpleChat.Network;
using SimpleChat.Utilities;
using SimpleChat.VM.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace SimpleChat.VM
{
    public class ChatManagerVM : INotifyPropertyChanged
    {
        private Dispatcher dispatcher;
        private ChatManager chatManager;
        private ObservableCollection<Message> chatMessages;
        private string hostIp;
        private int hostPort;
        private string statusInfo;
        private bool initialized;
        private TitleObjectVM titleObject;
        private string currentMessage;

        public ChatManagerVM()
        {
            // dispatcher.Invoke(() => Items.Add(item)
            this.dispatcher = Dispatcher.CurrentDispatcher;
            this.ChatMessages = new ObservableCollection<Message>();
            this.chatManager = new ChatManager();
            this.chatManager.Connected += this.Manager_Connected;
            this.chatManager.Disconnected += this.Manager_Disconnected;
            this.chatManager.Listening += this.Manager_Listening;
            this.chatManager.Stopped += this.Manager_Stopped;
            this.chatManager.MessageAdded += this.Manager_MessageAdded;
            this.chatManager.NewChatLog += this.ChatManager_NewChatLog;
            //this.chatManager.MessageReceived += this.Manager_MessageReceived;

            this.initialized = false;
            this.OwnPort = 1337;
            this.HostPort = 1338;
            this.HostIp = "127.0.0.1";
            this.StatusInfo = "test";

            this.TitleObject = new TitleObjectVM()
            {
                HostIp = "127.0.0.1",
                HostUserName = "JohnDoe",
                Status = "Test"
            };

            this.InitializeCommands();

            //for (int i = 0; i < 10; i++)
            //{
            //    this.chatMessages.Add(new Message(i.ToString().PadLeft(200,'o'), true));
            //    this.chatMessages.Add(new Message(i.ToString().PadLeft(20, 'o'), false));
            //}
        }

        public event EventHandler<ChatManagerVMExitedEventArgs> Exited;

        /// <summary>
        /// The <see cref="PropertyChangedEventHandler"/> of this class.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        
        public string StatusInfo
        {
            get
            {
                return this.statusInfo;
            }

            private set
            {
                this.statusInfo = value;
                this.Notify(nameof(this.StatusInfo));
            }
        }

        public TitleObjectVM TitleObject
        {
            get
            {
                return this.titleObject;
            }

            private set
            {
                this.titleObject = value;
                this.Notify(nameof(this.TitleObject));
            }
        }

        public string UserName
        {
            get
            {
                return this.chatManager.UserName;
            }

            set
            {
                if (this.chatManager == null)
                {
                    return;
                }

                this.chatManager.UserName = value;
                this.Notify(nameof(this.UserName));
                this.Notify(nameof(this.IsOwnNamePortValid));
            }
        }
        
        internal void StartListening()
        {
            this.chatManager.StartListening();
        }

        internal void Exit()
        {
            throw new NotImplementedException();
        }

        public int OwnPort
        {
            get
           {
                return this.chatManager.Port;
            }

            set
            {
                this.chatManager.Port = value;
                this.Notify(nameof(this.OwnPort));
                this.Notify(nameof(this.IsOwnNamePortValid));
            }
        }

        internal bool SetupSession(object argument, BackgroundWorker worker, DoWorkEventArgs e)
        {
            // Send user name.
            if (!worker.CancellationPending)
            {
                worker.ReportProgress(10, new Tuple<string,bool>("Sending your username to your partner..", false));

                if (this.chatManager.SendUserName())
                {
                    worker.ReportProgress(20, new Tuple<string, bool>("Username sent..",false));
                }
                else
                {
                    throw new IOException("Cannot send username.");
                }

            }
            else
            {
                e.Cancel = true;
                return false;
            }

            // Wait for partner user name.
            if (!worker.CancellationPending)
            {
                worker.ReportProgress(40, new Tuple<string, bool>("Asking your partner for its username..",false));

                for (int i = 0; i < 10000; i+=100)
                {
                    worker.ReportProgress(40, new Tuple<string, bool>($"Waiting for response..{(int)Math.Ceiling((double)i / 1000)} s / 10 s ..", true));

                    if (this.initialized)
                    {
                        break;
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }

                if (!this.initialized)
                {
                    throw new TimeoutException("No response from your partner");
                }
            }
            else
            {
                e.Cancel = true;
                return false;
            }

            worker.ReportProgress(60, new Tuple<string, bool>($"Your partner is: {this.HostUserName}", false));

            // Wait for partner user name.
            if (!worker.CancellationPending)
            {
                worker.ReportProgress(80, new Tuple<string, bool>($"Looking for old messages between you and {this.HostUserName}..", true));

                if (this.chatManager.LoadLogFor(this.HostUserName, this.HostIp))
                {
                    worker.ReportProgress(95, new Tuple<string, bool>($"Previous messages loaded..", false));
                }
                else
                {
                    worker.ReportProgress(95, new Tuple<string, bool>($"There aren't any old messages..", false));
                }
            }
            else
            {
                e.Cancel = true;
                return false;
            }

            return true;
        }

        public bool IsOwnNamePortValid
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.UserName) && this.OwnPort.IsBetween(1025, ushort.MaxValue);//&& int.TryParse(this.Port, out int res) && res.IsBetween(1025, ushort.MaxValue);
            }
        }

        public string HostIp
        {
            get
            {
                return this.hostIp;
            }

            set
            {
                this.hostIp = value;
                this.Notify(nameof(this.HostIp));
                this.Notify(nameof(this.IsHostIpPortValid));
                this.TitleObject.Status = value;
                this.Notify(nameof(this.TitleObject));
            }
        }

        public int HostPort
        {
            get
            {
                return this.hostPort;
            }

            set
            {
                this.hostPort = value;
                this.Notify(nameof(this.HostPort));
                this.Notify(nameof(this.IsHostIpPortValid));
            }
        }

        public bool IsHostIpPortValid
        {
            get
            {
                return this.HostIp.IsValidIpv4Address() && this.HostPort.IsBetween(1025, ushort.MaxValue);
            }
        }

        public string HostUserName
        {
            get
            {
                return this.chatManager.HostUserName;
            }
        }

        public ObservableCollection<Message> ChatMessages
        {
            get
            {
                return this.chatMessages;
            }

            private set
            {
                if (value == null)
                {
                    return;
                }

                this.chatMessages = value;
                this.Notify(nameof(this.ChatMessages));
            }
        }

        public NetworkStatus NetworkStatus
        {
            get
            {
                if (this.chatManager == null)
                {
                    return NetworkStatus.Stopped;
                }

                return this.chatManager.NetworkStatus;
            }
        }

        public RelayCommand<ChatManagerVM> ConnectCommand
        {
            get;
            private set;
        }
        public RelayCommand<ChatManagerVM> SendCommand { get; private set; }

        /// <summary>
        /// Fires the property changer event of this class, it also makes sure that the UI will be called on the UI thread.
        /// </summary>
        /// <param name="propName">The name of the property which has changed.</param>
        protected virtual void Notify([CallerMemberName]string propName = null)
        {
            // todo: check, if throws error then try with saved dispatcher at teh beginning  
            if (Application.Current.Dispatcher.CheckAccess())
            {
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(
                  DispatcherPriority.Background,
                  new Action(() => {
                      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
                  }));
            }


            // this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        protected virtual void FireExited(object sender, ChatManagerVMExitedEventArgs e)
        {
            this.Exited?.Invoke(sender, e);
        }
        
        private void InitializeCommands()
        {
            this.ConnectCommand = new RelayCommand<ChatManagerVM>(param => this.Connect(), param => this.CanConnect());
            this.SendCommand = new RelayCommand<ChatManagerVM>(param => this.SendMessage(), param => this.CanSend());
        }

        private bool CanSend()
        {
            return this.NetworkStatus == NetworkStatus.Connected;
        }

        private void SendMessage()
        {
            var msg = this.CurrentMessage;
            this.CurrentMessage = string.Empty;
            this.chatManager.SendMessage(msg);
        }

        private void Connect()
        {
            this.chatManager.Connect(this.HostIp, this.HostPort);
        }

        private bool CanConnect()
        {
            return this.NetworkStatus != NetworkStatus.Connected;
        }

        private void AddMessage(Message message)
        {
            lock (this.ChatMessages)
            {
                try
                {
                    var list = this.ChatMessages.ToList<Message>();
                    list.Add(message);

                    this.ChatMessages = new ObservableCollection<Message>(list);

                    try
                    {
                        this.TitleObject.Status = message.Text.Substring(0, Math.Min(message.Text.Length, 10)) + "...";
                        this.Notify(nameof(this.TitleObject));
                    }
                    catch
                    {
                    }
                }
                catch
                {
                }
            }            
        }
        
        private void Manager_Stopped(object sender, Network.NetworkManagerStoppedEventArgs e)
        {
            this.Notify(nameof(this.NetworkStatus));
            this.TitleObject.Status = this.chatManager.NetworkStatus.ToString();
            this.Notify(nameof(this.TitleObject));
        }

        private void Manager_MessageAdded(object sender, ChatManagerChatLogMessageAddedEventArgs e)
        {
            if (this.initialized)
            {
                if (e != null)
                {
                    this.AddMessage(e.Message);
                }
            }
            else
            {
                this.initialized = true;
                this.TitleObject.HostUserName = this.HostUserName;
                this.Notify(nameof(this.HostUserName));
                this.Notify(nameof(this.TitleObject));
            }
        }

        private void ChatManager_NewChatLog(object sender, ChatManagerNewChatLogEventArgs e)
        {
            this.ChatMessages = new ObservableCollection<Message>(this.chatManager.ChatLog.MessageList);
        }

        private void Manager_Listening(object sender, Network.NetworkManagerListeningEventArgs e)
        {
            this.Notify(nameof(this.NetworkStatus));
        }

        private void Manager_Disconnected(object sender, Network.NetworkManagerDisconnectedEventArgs e)
        {
            this.Notify(nameof(this.NetworkStatus));
            this.TitleObject.Status = this.chatManager.NetworkStatus.ToString();
            this.Notify(nameof(this.TitleObject));
        }

        private void Manager_Connected(object sender, Network.NetworkManagerConnectedEventArgs e)
        {
            if (e != null && e.Ip != null)
            {
                this.HostIp = e.Ip;
            }
            
            this.Notify(nameof(this.NetworkStatus));

            this.TitleObject.Status = this.chatManager.NetworkStatus.ToString();
            this.Notify(nameof(this.TitleObject));
        }

        public string CurrentMessage
        {
            get
            {
                return this.currentMessage;
            }

            set
            {
                this.currentMessage = value;
                this.Notify(nameof(this.CurrentMessage));
            }
        }
    }
}