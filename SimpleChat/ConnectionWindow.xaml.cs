﻿using SimpleChat.Network;
using SimpleChat.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SimpleChat
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    public partial class ConnectionWindow : Window
    {
        public ConnectionWindow()
        {
            InitializeComponent();
            this.DataContext = (Application.Current as App).ManagerVM;
            Application.Current.MainWindow.Height = (int)Math.Max(150, Math.Round(SystemParameters.PrimaryScreenHeight / 6));
            Application.Current.MainWindow.Width = (int)Math.Max(300, Math.Round(SystemParameters.PrimaryScreenWidth / 4));
            (Application.Current as App).ManagerVM.PropertyChanged += this.ManagerVM_PropertyChanged;
            (Application.Current as App).ManagerVM.StartListening();
        }

        private void ManagerVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e != null && e.PropertyName == nameof(ChatManagerVM.NetworkStatus) && (Application.Current as App).ManagerVM.NetworkStatus == NetworkStatus.Connected)
            {
                //MessageBox.Show("You're connected to a chat partner at:\n" + (Application.Current as App).ManagerVM.HostIp, "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                Window nextWindow = new ChatWindow();
                nextWindow.Show();
                (Application.Current as App).ManagerVM.PropertyChanged -= this.ManagerVM_PropertyChanged;
                this.Close();
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                NameBox.MaxWidth = Math.Max(1, Application.Current.MainWindow.ActualWidth - NameLabel.ActualWidth);
                NameBox.Width = Math.Max(1, Application.Current.MainWindow.ActualWidth - NameLabel.ActualWidth);
            }
            catch
            {
            }
        }
    }
}
