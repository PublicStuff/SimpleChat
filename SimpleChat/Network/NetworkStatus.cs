﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleChat.Network
{
    public enum NetworkStatus
    {
        Stopped,
        // Started,
        Listening,
        Connected
    }
}