﻿using SimpleChat.Chat;
using SimpleChat.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChat.Network
{
    public class NetworkManager
    {
        private bool exit;
        private Task workerTask;
        private NetworkIO networkIO;
        private int port;

        public NetworkManager(int port = 1337)
        {
            this.Port = port;
            this.exit = false;
        }

        public event EventHandler<NetworkIOMessageReceivedEventArgs> MessageReceived;
        public event EventHandler<NetworkManagerListeningEventArgs> Listening;
        public event EventHandler<NetworkManagerStoppedEventArgs> Stopped;
        public event EventHandler<NetworkManagerConnectedEventArgs> Connected;
        public event EventHandler<NetworkManagerDisconnectedEventArgs> Disconnected;

        public NetworkStatus Status
        {
            get
            {
                if (this.networkIO == null || !this.networkIO.IsRunning)
                {
                    if (this.workerTask != null && this.workerTask.Status == TaskStatus.Running)
                    {
                        return NetworkStatus.Listening;
                    }
                    else
                    {
                        return NetworkStatus.Stopped;
                    }
                }
                else
                {
                    return NetworkStatus.Connected;
                }
            }
        }

        public int Port
        {
            get
            {
                return this.port;
            }

            private set
            {
                if (value.IsBetween(1025, ushort.MaxValue))
                {
                    this.port = value;
                }
            }
        }

        public bool Send(Message message)
        {
            if (this.networkIO != null && this.Status == NetworkStatus.Connected)
            {
                return this.networkIO.Send(message);
            }
            else
            {
                return false;
            }
        }

        public void StartListening()
        {
            if (this.Status == NetworkStatus.Stopped)
            {
                this.exit = false;
                this.workerTask = new Task(this.ListenerWorker);
                this.workerTask.Start();
            }
        }

        public void StopListening()
        {
            if (this.Status == NetworkStatus.Listening)
            {
                this.exit = true;
            }
        }

        public void Connect(string hostname, int port, bool stopListening = false)
        {
            if ((this.Status != NetworkStatus.Listening || stopListening) && this.Status != NetworkStatus.Connected)
            {
                TcpClient client = null;

                try
                {
                    client = new TcpClient(hostname, port);
                }
                catch
                {
                    this.FireDisconnected(this, new NetworkManagerDisconnectedEventArgs());
                    this.FireStopped(this, new NetworkManagerStoppedEventArgs());
                    return;
                }

                this.networkIO = new NetworkIO(client,this.Port);

                this.networkIO.Error += this.NetworkIO_Error;
                this.networkIO.MessageReceived += this.NetworkIO_MessageReceived;
                this.networkIO.Started += this.NetworkIO_Started;
                this.networkIO.Stopped += this.NetworkIO_Stopped;

                this.networkIO.Start();
                this.FireConnected(this, new NetworkManagerConnectedEventArgs(null));
            }
        }

        public void Disconnect()
        {
            if (this.Status == NetworkStatus.Connected && this.networkIO != null)
            {
                this.networkIO.Stop();
            }
        }

        private void NetworkIO_Stopped(object sender, NetworkIOStoppedEventArgs e)
        {
            if (this.networkIO != null)
            {
                this.networkIO.Error -= this.NetworkIO_Error;
                this.networkIO.MessageReceived -= this.NetworkIO_MessageReceived;
                this.networkIO.Started -= this.NetworkIO_Started;
                this.networkIO.Stopped -= this.NetworkIO_Stopped;
            }

            this.networkIO = null;

            this.FireDisconnected(this, new NetworkManagerDisconnectedEventArgs());
            this.FireStopped(this, new NetworkManagerStoppedEventArgs());
        }

        private void NetworkIO_Started(object sender, NetworkIOStartedEventArgs e)
        {
            // this.FireConnected(this, new NetworkManagerConnectedEventArgs());
        }

        private void NetworkIO_MessageReceived(object sender, NetworkIOMessageReceivedEventArgs e)
        {
            this.FireMessageReceived(this, e);
        }

        private void NetworkIO_Error(object sender, NetworkIOErrorEventArgs e)
        {
            if (this.networkIO != null)
            {
                this.networkIO.Stop();
            }
        }
        
        protected virtual void FireMessageReceived(object sender, NetworkIOMessageReceivedEventArgs e)
        {
            this.MessageReceived?.Invoke(sender, e);
        }

        protected virtual void FireListening(object sender, NetworkManagerListeningEventArgs e)
        {
            this.Listening?.Invoke(sender, e);
        }

        protected virtual void FireConnected(object sender, NetworkManagerConnectedEventArgs e)
        {
            this.Connected?.Invoke(sender, e);
        }

        protected virtual void FireDisconnected(object sender, NetworkManagerDisconnectedEventArgs e)
        {
            this.Disconnected?.Invoke(sender, e);
        }

        protected virtual void FireStopped(object sender, NetworkManagerStoppedEventArgs e)
        {
            this.Stopped?.Invoke(sender, e);
        }

        public void ListenerWorker()
        {
            TcpListener listener;

            try
            {
                listener = new TcpListener(IPAddress.Any, this.Port);
                listener.Start();
            }
            catch
            {
                this.exit = true;
                this.FireStopped(this, new NetworkManagerStoppedEventArgs());
                return;
            }

            Task.Run(() =>
            {
                this.FireListening(this, new NetworkManagerListeningEventArgs());
            });

            TcpClient client = null;

            while (!this.exit)
            {
                Task.Delay(100).Wait();

                if (listener.Pending())
                {
                    client = null;

                    try
                    {
                        client = listener.AcceptTcpClient();
                    }
                    catch
                    {
                        continue;
                    }

                    try
                    {
                        if (this.networkIO != null)
                        {
                            this.networkIO.Error -= this.NetworkIO_Error;
                            this.networkIO.MessageReceived -= this.NetworkIO_MessageReceived;
                            this.networkIO.Started -= this.NetworkIO_Started;
                            this.networkIO.Stopped -= this.NetworkIO_Stopped;
                            this.networkIO.Stop();
                        }

                        this.networkIO = null;

                        this.networkIO = new NetworkIO(client,this.Port);

                        this.networkIO.Error += this.NetworkIO_Error;
                        this.networkIO.MessageReceived += this.NetworkIO_MessageReceived;
                        this.networkIO.Started += this.NetworkIO_Started;
                        this.networkIO.Stopped += this.NetworkIO_Stopped;

                        this.networkIO.Start();
                    }
                    catch
                    {
                        continue;
                    }

                    listener.Stop();
                    this.exit = true;
                }
            }

            Task.Run(() =>
            {
            this.FireConnected(this, new NetworkManagerConnectedEventArgs(((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString()));
            });
        }
    }
}