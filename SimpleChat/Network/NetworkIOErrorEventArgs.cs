﻿using System;

namespace SimpleChat.Network
{
    public class NetworkIOErrorEventArgs : EventArgs
    {
        public NetworkIOErrorEventArgs(string message = "")
        {
            this.Message = message ?? throw new ArgumentNullException(nameof(this.Message));
        }

        public string Message { get; }
    }
}