﻿using SimpleChat.Chat;
using System;

namespace SimpleChat.Network
{
    public class NetworkIOMessageReceivedEventArgs : EventArgs
    {
        public NetworkIOMessageReceivedEventArgs(Message message)
        {
            this.Message = message;
        }

        public Message Message
        {
            get;
        }

        public DateTime Timestamp
        {
            get
            {
                return this.Message.Timestamp;
            }
        }

        //public NetworkIOMessageReceivedEventArgs(byte[] bytes)
        //{
        //    this.Bytes = bytes ?? throw new ArgumentNullException(nameof(this.Bytes));
        //}

        //public byte[] Bytes { get; }
    }
}