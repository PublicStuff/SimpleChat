﻿using SimpleChat.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SimpleChat.Network
{
    public class NetworkIO
    {
        private bool exit;
        private int bufferSize;

        private Task readerTask;

        public NetworkIO(TcpClient tcpClient, int port = 1337, int bufferSize = 8192)
        {
            this.Port = port;
            this.BufferSize = bufferSize;
            this.Client = tcpClient ?? throw new ArgumentNullException(nameof(this.Client));
        }

        public event EventHandler<NetworkIOMessageReceivedEventArgs> MessageReceived;
        public event EventHandler<NetworkIOErrorEventArgs> Error;
        public event EventHandler<NetworkIOStartedEventArgs> Started;
        public event EventHandler<NetworkIOStoppedEventArgs> Stopped;

        public TcpClient Client { get; private set; }

        public NetworkStream Stream { get; private set; }

        public int BufferSize
        {
            get
            {
                return this.BufferSize;
            }

            private set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(this.BufferSize));
                }

                this.bufferSize = value;
            }
        }

        public bool IsRunning
        {
            get
            {
                if (this.readerTask != null && this.readerTask.Status == TaskStatus.Running)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public int Port
        {
            get;
        }

        public void Start()
        {
            if (this.IsRunning)
            {
                return;
            }

            this.exit = false;
            this.readerTask = new Task(this.Worker);

            try
            {
                this.readerTask.Start();
            }
            catch
            {
                this.readerTask = null;
                this.FireError(this, new NetworkIOErrorEventArgs("Network could not be started."));
            }
        }

        public void Stop()
        {
            if (!this.IsRunning)
            {
                return;
            }

            this.exit = true;
        }

        public bool Send(Message message)
        {
            try
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(this.Stream, message);
            }
            catch
            {
                this.exit = true;
                this.FireError(this, new NetworkIOErrorEventArgs("Could not write into stream."));
                return false;
            }

            return true;
        }
        public void Worker()
        {
            try
            {
                this.Stream = this.Client.GetStream();
            }
            catch
            {
                this.exit = true;
                this.FireError(this, new NetworkIOErrorEventArgs("Could not connect to client."));
            }

            Task.Run(() =>
            {
                this.FireStarted(this, new NetworkIOStartedEventArgs());
            });

            BinaryFormatter binaryFormatter = new BinaryFormatter();

            while (!this.exit)
            {
                if (this.Stream == null)
                {
                    this.exit = true;
                    break;
                }

                if (this.Stream.DataAvailable)
                {
                    Message message = null;

                    try
                    {
                        message = (Message)binaryFormatter.Deserialize(this.Stream);
                        message.OwnMessage = false;
                    }
                    catch
                    {
                        this.FireError(this, new NetworkIOErrorEventArgs("Could not deserialize message."));
                        this.exit = true;
                        break;
                    }

                    this.FireMessageReceived(this, new NetworkIOMessageReceivedEventArgs(message));
                }

                Task.Delay(100).Wait();
            }

            this.Stream.Dispose();
            this.Client.Dispose();

            this.FireStopped(this, new NetworkIOStoppedEventArgs());
        }
        //public void Worker()
        //{
        //    try
        //    {
        //        this.Stream = this.Client.GetStream();
        //    }
        //    catch
        //    {
        //        this.exit = true;
        //        this.FireError(this, new NetworkIOErrorEventArgs("Could not connect to client."));
        //    }

        //    Task.Run(() => 
        //    {
        //        this.FireStarted(this, new NetworkIOStartedEventArgs());
        //    });

        //    while (!this.exit)
        //    {
        //        if (this.Stream == null)
        //        {
        //            this.exit = true;
        //            break;
        //        }

        //        if (this.Stream.DataAvailable)
        //        {
        //            byte[] byteArray = new byte[8192];

        //            try
        //            {
        //                this.Stream.Read(byteArray, 0, 8192);
        //            }
        //            catch
        //            {
        //                this.FireError(this, new NetworkIOErrorEventArgs("Could not write to stream."));
        //                this.exit = true;
        //                break;
        //            }

        //            this.FireMessageReceived(this, new NetworkIOMessageReceivedEventArgs(byteArray));
        //        }

        //        Task.Delay(100).Wait();
        //    }

        //    this.Stream.Dispose();
        //    this.Client.Dispose();

        //    this.FireStopped(this, new NetworkIOStoppedEventArgs());
        //}

        protected virtual void FireError(object sender, NetworkIOErrorEventArgs e)
        {
            this.Error?.Invoke(sender, e);
        }

        protected virtual void FireMessageReceived(object sender, NetworkIOMessageReceivedEventArgs e)
        {
            this.MessageReceived?.Invoke(sender, e);
        }

        protected virtual void FireStarted(object sender, NetworkIOStartedEventArgs e)
        {
            this.Started?.Invoke(sender, e);
        }

        protected virtual void FireStopped(object sender, NetworkIOStoppedEventArgs e)
        {
            this.Stopped?.Invoke(sender, e);
        }
    }
}