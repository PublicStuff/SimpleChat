﻿using System;

namespace SimpleChat.Network
{
    public class NetworkManagerConnectedEventArgs : EventArgs
    {
        public NetworkManagerConnectedEventArgs(string ip)
        {
            this.Ip = ip;
        }

        public string Ip { get; }
    }
}